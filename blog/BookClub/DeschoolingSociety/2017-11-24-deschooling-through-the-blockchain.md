---
title: Deschooling and the blockchain
date: 2017-11-07
author: Jared Pereira
---

Illich proposed a bunch of practical systems for building the educational
networks he describes. For example, he described a matchmaking systems, to
facilliate incidental learning among peers.

### Educational Registries
> Each man, at any given moment and at a minimum price, could identify himself
> to a computer with his address and telephone number, indicating the book,
> article, film, or recording on which he seeks a partner for discussion. Within
> days he could receive by mail the list of others who recently had taken the
> same initiative. This list would enable him by telephone to arrange for a
> meeting with persons who initially would be known exclusively by the fact that
> they requested a dialogue about the same subject.

This is a simple powerful idea. It's a system to enable individuals to "share an
issue which for them, at the moment, is socially, intellectually, and
emotionally important", as Illich put it, and to grow from it as a result.

An important aspect of this, and many of Illich's proposals in general, is the
scale. Such systems are only valuable in _large_ communities, and only at such a
point that the network effects kick in. No one would want to, or rather no one
would, participate in a matchmaking service that no one else was on, nor buy
into a road system that didn't lead everywhere.

### How can it be built? 

The normal answer would be the government, but here the result of the
government's work is exactly what Illich is so critical towards. Having it in
control of such a system would _at best_ be a huge privacy concern and at worst
a little more insidious.

The dotcom age that was just emerging with network systems in the 70s could
provide a different answer. Why couldn't a startup, or some hackers, build this
service? ->the matchmaking, not the public trans service

Well firstly I think the internet _did_ improve the situation. It led to an
explosion of niche social spaces, for people to congregate around ideas and
interests, and to learn from each other without any central authority
controlling their topics or experiences. However, these niches could never
scale.

Though the system works in small communities, as soon as it grows the
self-enforcing cultural norms fall apart. Individuals need to have trust in the
experiences of their peers in order to engage with them. So why can't we have
that in large systems?

I think the answer is why blockchain systems are so useful. The internet enabled
wide spread communication, _but not coordination_. The large scale social
networks that emerged are generally information dumps, _not_ the kind of
rule-based social environments Illich needed for learning webs.

## Enter Blockchains
It's fascinating to notice the connections between the criticisms that Illich
forwards and blockchain systems in general.

Specifically he talks about the idea of "conviviality": 

> autonomous and creative intercourse among persons, and the intercourse of
> persons with their environment; ... in contrast with the conditioned response
> of persons to the demands made upon them by others, and by a man-made
> environment.

And the question comes to mind, are blockchains convivial systems? I can see
both elements present in them. The transactional freedom they provide enables
direct communication and coordination between individuals. But also surely the
ideas of conditioned responses and external demands are present in the incentive
systems and singletons we're building.

I think this tension between freedom and constrait is central to the system,
it's what gives rise to strong social structures. And blockchain systems, with
their technological basis of individual agency and emergent property of strong
consensus, are poised to present a new paradigm here, one that's unclear and
under construction.

### How does this relate to learning?
A learner individually has almost complete freedom. They can build their own
experience however they want and shape themselves into the person they want to
be. But as soon as that individual's learning processs involves others they have
to subscribe to a larger system, so that they can be trusted, and so that they
can coordinate.

Illich desires one where the individual freedom is maintained, and _extended_
into social contexts, not (negatively) influenced by the system facillitating,
rejecting the "conditioned response of persons to the demands made upon them by
others". This is really hard.

But blockchains are promising. If learners have the freedom to build their own
pedagogical practices that all connect to a singular source of trust, they can
greatly lower coordination costs while still maintaining their freedom. This is
what we're trying to build.

## Playing around

But I think the relationship between Illich and this technology is more
fundamental than just fathom, or just education. He was essentially a visionary
societal architect, without the tools to construct that vision. And like him
there have been a _lot_ of thinkers throughout the years who explored the role
of institutions, and imagined varied and diverse systems.

Perhaps the decentralized systems we have today will be finally let us try some
of them out. It's going to be exciting to play. We definitely need to read and
learn more.
