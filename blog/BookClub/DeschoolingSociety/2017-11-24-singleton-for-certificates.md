---
title: A singleton for certificates
date: 2017-11-24
author: Julius Faber
---

 In Germany, where I live, we just celebrated the 500 years anniversary of the
 reformation. Back then Luther nailed his famous 95 theses to the door of the
 All-Saints Church in Wittenberg, among other criticizing the church's practice
 of selling indulgences that would relief the buyer of their sins so that they
 can go to heaven. Being the *only* one able to determine who had sinned while
 simultaneously being the *sole* source for relief from sins was too much power
 in one position.
 
 Since then, humanity has gone a long way: We established democracies who
 separate power into different organs controlling each other and we invented
 specialized mechanisms (e.g. cartel-laws) that try to prevent such situations
 of concentrated power from occurring.

 Everywhere? Ivan Illich, a clergyman, points out that for one very important
 aspect of our society (maybe the most important?) this is not the case:

 >"We permit the state to ascertain the universal educational deficiencies of
 >its citizens and establish one specialized agency to treat them."

 In this post, I want to explore this point, why it is so difficult to address
 and how "fathom" could contribute to finding a way out of it.

## The Need to De-School

Let me jump ahead: I think it was a mistake to do so. Schools are neither good
for establishing deficiencies, nor for treating them. Why? 

According to Illich, "education" has become the new *world religion*, a fact
that reflects in the ubiquitous desire of parents to send their children to
school, which is the only place (why else would schooling be obligatory?)
where one can get an education - and with it a way out of poverty and other
miseries of life. Yet, as Illich eloquently puts it, to equate education with
obligatory schooling is *"to confuse salvation with the church"*.

One of Illich's main criticisms, for which he was once called to Rome to justify
himself in front of the Vatican, is that the institutionalization of basic
needs, such as in the case of schools (education) or missionaries (faith), is
limiting an individual's freedom to fulfill those needs to the services that
those institutions are willing to provide to them - creating new ways of poverty
in the process. By increasingly relying on institutions, individuals learn to be
helpless. Instead Illich argues to shift the responsibility for learning back to
the learner - that we shall take education in our own hands, establishing or own
'deficiencies' in that sense and seek out ways to address them.

And ways to address them there are many! Illich lucidly describes a range of
 alternative teaching and learning methods (read an overview
 [here](INSERTGENIUSLINK)) and backs them up with inspiring real-world examples,
 such as how a friend of his recruited a bunch of teenagers, trained them for a
 week in the use of an instructive manual originally designed for
 linguist-graduates and had them successfully teach Spanish to some 300+ members
 of his Archdiocese so that they could deal with a sudden surge of Puerto-Rican
 immigrants. 

## Towards a new kind of education

In order for people to become more open to his new educational opportunities and
 these stories to become the rule rather than the exception, he suggests a very
 radical measure: Akin to the first amendment which forbade the state to meddle
 in an individual's religion, Illich suggests that *"the state shall make no law
 with respect to the establishment of an education"*. In a similar vein, he
 proposed that to inquiries into an individual's racial background or their
 bedroom habits, inquiries into a someone's learning history would be illegal.
 It would be still okay to do tests to establish whether one has the necessary
 skills for a position, but illegal to require that he attained it in a certain
 way.

While this would most certainly make people pursue educational opportunities
outside of school, implementing such an idea would be a huge challenge. But
putting the general feasibility of such ideas aside for now, there is
another problem here: Certificates are really useful for coordination, so people
would still want to have them. With no 'official' (government-backed) learning
paths (and respective certificates) or any laws that assure the quality of
educational claims their number would grow and grow into a jungle of different
certificates and tests that no individual could make sense of by themselves.

This lack of a single source of truth would create a need for
'certificate-validating' institutions - ultimately leading again to a
centralization of trust and thus to the same problems that motivated the
abolishing of schools in the first place.

## Decentralizing certification

Without a decentralized, truly inclusive way of issuing and verifying
credentials, Illich's vision of a deschooled society can not come to fruition.
And this is where, we hope, fathom comes into play: Providing certificates
generated from a social process implemented on a blockchain, fathom can act as a
single point of reference and trust. Fathom is inclusive and scalable because anybody
can be assessed and become an assessor. There is no limit to how many people can
use a peer-to-peer social protocol to interact with each other!

By specifying how different assessments of the same skill can be related to each
other, it leaves it open to anybody's interpretation how someone's proficiency
at a skill shall be established, allowing for a wide diversity of assessment
styles. For example, a fathom certificate backed by 5 assessors is essentially
saying that five persons have used the method they think is most appropriate and
have come to the same conclusion.

The process is entirely topic-agnostic and therefore we hope that whatever
community is in need of a credential, it will be able to use fathom for it. Once
enough people are using the system, anybody able to clearly define what a
credential entails, should be able to find assessors for it.

Then, hopefully, people will be able to learn however they want, whatever they
want.

-julius
