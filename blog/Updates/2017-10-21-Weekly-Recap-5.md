---
title: Weekly Recap 5
date: 2017-10-21
---
## Chat rooms
Michiya has been working on setting up a RethinkDB server to store message history for the chatrooms. This will lead into refactoring the front-end using React+Redux in order to enable some of the more interesting features we're planning.

## Whitepaper
We've been combing over the whitepaper, generally reviewing and rephrasing. We hope to share it on a small scale within a week, and continue iterating on it. You can view progress on this [branch](https://gitlab.com/fathom/fathom.gitlab.io/tree/whitepaper).

## Book Club
One week may have been a tad ambitous. As well as trying to do Deschooling Society which has just a crazy amount of ideas stuffed in there. It might be worth it to split it up into a couple different sections. Either way, the discussions will continue and hopefully we'll publish some thoughts this week. 

## General Roadmap
This week's update is a bit short as most of the work we're doing is incremental progress to some big features/releases we're bringing up over the next couple weeks. The 2 big things are the whitepaper, and a properly set up chat up. There'll be a lot we can do with all of this, and though the work may not be super flashy at the moment we _are_ super excited about it all.

