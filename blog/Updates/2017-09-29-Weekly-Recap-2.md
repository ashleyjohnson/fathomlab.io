---
title: Weekly Recap 2
date: 2017-09-29
---

Streak of 2! Our commitment is to be doing these even when we got less done than we planned so hopefully all goes well. 

## Website:
We've been working on a revamp of the website, including some fun new graphics and new styling. You can keep track of some of that on [this](https://gitlab.com/fathom/fathom.gitlab.io/merge_requests/11) merge request, though it is still very much a work in progress. Timeline for this is done mid-next week. 

## Assess codebase
We've started working on the backlog of issues in the assessment protocol. 
The three major issues/features are:

1. Redistributing funds from dissenting assessors [merge request !65](https://gitlab.com/fathom/assess/merge_requests/65)

2. Refactoring token flow through assessments [merge request !66](https://gitlab.com/fathom/assess/merge_requests/66)

3. Dealing with tied assessments [merge request !64](https://gitlab.com/fathom/assess/merge_requests/64)

## Chatrooms!
Lot's of little things happening here. This week has been mostly on ease-of-use fixes and features rather than fundamentally new interaction mechanisms but hopefully those are coming next week! 

We added: 

- [markdown parsing](https://gitlab.com/fathom/chat/commit/f658bcfbe0616bda0d3a60580726bafb3d8d55e8)
- [notifying transaciton recipient](https://gitlab.com/fathom/chat/commit/2ff75b1ea15fa9316e4aba16901e506b3418150f) and [validating transaction hashes](https://gitlab.com/fathom/chat/commit/2ff75b1ea15fa9316e4aba16901e506b3418150f)
- added more config details for [networks](https://gitlab.com/fathom/chat/commit/01e6df46c31f4cb4afc1afaf9e8a0e6576d24f22) and their associate [etherscans](https://gitlab.com/fathom/chat/commit/a00b215cce02a0123f3ea3088343a8cc71187eaf)

The plan is to enable first chatroom metadata stored in JSON in IPFS, and then use that to enable contract interactions. The latter will probably take more time, but hopefully a simple version will be usuable. 

The plan is to formalize a roadmap for this sometime in the next week as well. There's a lot of really cool little things I'd like to do but the challenge will be in enabling the most of those with the smallest amount of features. We'll see where it takes us. 


## Whitepaper
Julius has been working on fleshing out sections of the whitepaper. You can keep track of that work [here](https://gitlab.com/fathom/fathom.gitlab.io/tree/whitepaper/whitepaper). It's still extremely scattered but it coming together.

Next week we plan to move this into a more formal final structure as well as develop some visual assetts to go along with it. 

## Community
Both myself (Jared) and Julius, have been reading [Deschooling Society](http://www.davidtinapple.com/illich/1970_deschooling.html) by Ivan Illich (which you should definitely read) and it's kinda been blowing our minds. This prompted the idea of a weekly or biweekly book club, where we read works related to fathom and publish our thoughts and solicity others. This would probably be in the form of a mailing list coupled with a post on this blog but we're still figuring out exactly how to set up it. More next week! 

------
And that's it folks! 
