---
title: 'New Years Update: Constructing Constructionist Communities'
date: 2018-01-23
---
Our work has been on two fronts, specifying the Fathom Playground (last
mentioned way back
[here](https://fathom.network/blog//Updates/2017-11-17-Weekly-Recap-6.html) and
improving the chatroom to support it.

## The Playground
The Fathom Playground (previously mentioned as a Digital Learners Residency ) is
a full-time remote learning program around decentralized systems. It is built
around self-directed learning and user constructed pedagogical environments.

The idea is to have a set of people motivated to learn about decentralized
systems and a minimal set of rules to allow them to coordinate with each other
for that learning. 

We're fleshing out what those rules are, and other details around the whole
program in [this
MR](https://gitlab.com/fathom/fathom.gitlab.io/merge_requests/31/diffs). The
goal for these pages is to introduce the program and concepts so that they can
be iterated on and discussed.

### Constructionism
An important pedagogical perspective to us is that of
[Construcitonism](https://en.wikipedia.org/wiki/Constructionism_(learning_theory)). 
The Playground is not about passing on models (partially because we haven't
really figured them out yet) but creating an environment in which individuals
can collaborate to create them.

## More chatrooms!
We're working on making the chatroom application less of a standalone object and
more a full-featured environment. This means allowing for multiple rooms in a
single window, moving this more into traditional chat environment territory. 

We're also working on implementing better information management in the UI,
adding a sidebar for room details as well as smart-contracts and room
management. Figuring out the information hierarchies and layout of it all is the
current challenge. 

### Better user management
We are also working on getting a better login flow in place to reduce the friction of
interacting in multiple rooms.

This work is currently going on in the [develop branch](https://gitlab.com/fathom/chat/tree/develop).

### Parsing
While our slash commands are _fun_ they're not exactly effective. We have a
monster of a parsing function that just splits a given command into words,
(based on spaces) and then has a switch statement to determine what to do. You
can take a look at it
[here](https://gitlab.com/fathom/chat/blob/master/app/components/App.jsx#L122)
and you can look at our messy command functions
[here](https://gitlab.com/fathom/chat/blob/master/app/commands.js) 

#### Just JS
In thinking about how to better tackle this problem we realized we had just
re-implemented interfaces for javascript we had in our source objects. So, why
not just have the user execute javascript themselves? 

We have can have isomorphism between the interaction environment and development
environment, allowing users to contribute back to the codebase easily as well as
engage in DApp development.

You can take a look at this work on this
[MR](https://gitlab.com/fathom/chat/merge_requests/41) 

### Better Backend
We're also continuing to clean up the backend using socket.io. The main
advantage is better connection handling and maintenance as well as neater room
management. 

We are also working to clean up our message schemas to be more maintainable and
less messy.

## The Book Club
Our team has been a little spread out over the last little bit and so our
reading has stalled. Some of us have been digging into Mindstorms by Seymour
Papert, so that will likely be our next book. And man is it a fantastic one.
