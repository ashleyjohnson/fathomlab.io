---
title: Weekly Recap 6
date: 2017-11-17
---

We're back! We've been a little scattered since DevCon but have been taking time to regroup and plan the next steps forward.

## Next steps forward:
We're focusing our efforts on refactoring the chat application to give it a stronger foundation moving forward. It will be enabling the next most step in our roadmap, a Digital Learners Residency (still working on finding the right name for it).

Essentially we plan to gather a group of people passionate about decentralized social systems to take part in a self-directed learning environment built around the fathom protocol. They will be able to set their own goals, create, and learn, all the while getting assessed; building their own reputation and contributing to the community. We're still fleshing out the details but we're super excited about it.

## The point
The goal here is to create a bootstrapping process for the entire fathom network, as well as a way to explore and create the ways that people will interact with it. More concrete definitions and plans around all this coming soon!

## What's goin on with the whitepaper?
Well, [it's up](https://fathom.network/whitepaper/)! We've been working on some enhancements of the build process. Previously we were maintaining two sources for the document, the markdown and the LaTeX, the former for the website and the latter for the PDF. We're now generating the PDF directly from the markdown.

This _does_ create some issues with links on the website, which hopefully we'll be able to resolve at some point. If anyone has any tips here, they would be appreciated.

### Not done yet
While the whitepaper is now public, it is by no means finished. There are a bunch of errors and typos yet to be caught, as well as places we could cut down on words or just clean up explanations. We figured it would be more useful to have the document public than to wait for it to be perfect.

If you find anything you think we should fix, please [create an issue](https://gitlab.com/fathom/fathom.gitlab.io/issues/new)

## Any contract work?
While we haven't been putting as much time into the core fathom protocol, we have been moving forward on how contracts interact with the chatrooms. A big part of this has been meta-rooms, which essentially allow you to define permissions functions that take additional arguments (aside from the address you want to look up). This let's us have diverse chatrooms with code deployed once. You can take a look at how this is implemented on [this branch](https://gitlab.com/fathom/chat/tree/addMetaRooms).

## The book club
We kinda let this lie for a while. Partially cause Ivan Illich takes some time to digest but also partially cause we didn't have a clear structure for it. In the next week or so we're compiling our thoughts in a couple separate blog posts, as well as some annotations around the work, most likely over at [genius](https://genius.com/). We'll then compile that into a blog post, and announce our next book.

You can catch some of this work in progress in this [merge request](https://gitlab.com/fathom/fathom.gitlab.io/merge_requests/23) and in the [drafts folder](https://fathom.network/_drafts/)

That's all folks!
