---
title: What does it (not) mean to have a fathom credential?
date: 2018-12-10
author: Julius Faber
---

__This post is the second of a series where I aim to explain how 
fathom-credentials are meaningful, even though the individuals involved in
creating them have no reputation, how this can lead to them not being "true" and
why that is not a problem but a feature. For the first post, click
[here](https://fathom.network/blog//2018-08-15-What-does-it-(not)-mean-to-have-a-credential.html)__

In the [previous
post](https://fathom.network/blog//2018-08-15-What-does-it-(not)-mean-to-have-a-credential.html)
of the series I stated that trust in a certificate can come from two possible
sources:

1. the process used to create them 
2. the person or institution administering the process

I concluded, that institutions have to rely on their reputation simply because
processes can not be verified at scale.

Enter blockchains. Blockchains provide a common source of truth and also make it possible to
create processes, called _smart-contracts_, that always execute exactly as designed.
With no downtime. Ever.

### Trust the process, not the persons running it

I want to say it again, differently: **We can now built technology that does not
belong to anybody!** 

Fathom tries to create a process that can be trusted without having to trust the
individuals executing it!

    
Briefly summarized, fathom implements a betting game in which a jury of
assessors judge an applicants skill, while backing up their judgements with
money. If a result is supported by a majority, the pot of money is redistributed
such that those assessors closest to the final result are rewarded the most.
Importantly, collusion among assessors is difficult to execute, due to how the
voting mechanism is architected: In order to collude, assessors must share their
votes with each other; but by doing so, they are sharing information their
fellow colluders could use to betray them and instead steal their money.
   
Put differently, the fathom protocol is a mechanism for individuals to stake
money to compare their subjective impression of someone's "mastery of Z" -- and be
rewarded if their views align.

### Why should you trust in that process? 

The idea that the members of the jury (assessors) are invested in the quality of
the result (they have "skin in the game") is what lends credibility to the
process: If I show you a certificate, it is not important who the assessors were
that created my certificate. What counts is that they have risked their own
resources and they did not loose them because the my proficiency was so
obvious that they all came to the same result[^3].

This principle of staking money in a coordination game is referred to as a
[schelling game](https://en.wikipedia.org/wiki/Focal_point_(game_theory)) and it
is important to acknowledge that the resilience of such methods against
malicious acting has yet to be proven.

Trusting a process is much harder than trusting a person/institution, so let's
have a closer look at some of its elements: 

### Implications of Relying on Subjectivity...

Let's get to the most important point right away:

#### ... for Truth (or certainty about it)

Aggregating subjective impressions is no recipe for finding the truth! In
fathom, assessors for a new assessment are those who have earner it earlier. In
case of a new credential, where there are on earlier certificate holders,
assessors are drawn from related credentials. And in the beginning, the first
assessors will just be some trustworthy individuals.

This means that the initial assessors and the first users will have a very large
influence of what it means to truly 'know Z'[^2].
Galileo would not have gotten a fathom physics-credential! 

This is very important to acknowledge, so I'll state it again: The
fathom-protocol is not for finding truth in the scientific sense. Instead it
allows communities that have certain standards (may they be scientifically
accurate or only coherent from inside a certain worldview) to map those onto a
set of credentials.

Most algorithms or processes have parameters and so does the fathom-protocol. Let's
look at how they affect it (and the truth)

####  ...for Credibility

So the fathom-process of creating credentials is not a silver bullet that will
[replace higher education](https://www.forbes.com/sites/jeffkauflin/2018/12/05/cryptopia-in-crisis-billionaire-joe-lubins-ethereum-experiment-is-a-mess-how-long-will-he-prop-it-up/#4778e4ed2f0a). It is limited in it's objectivity and
the scope of knowledge it can assess (e.g. cutting-edge scientific). This is
especially true for the beginning.

Yet, there are ways to address this and those are what make the protocol so
interesting:

1. It is possible to attach reputation to credentials by having assessors be non-anonymous.[^4]
2. It is possible to affect the degree of credibility by changing the number of assessors.
3. The price paid for an assessment allows to adjust the complexity of the examination.

If for example, 3 out of 5 anonymous assessors (you would only see their
ethereum addresses) would attest that I have a solid knowledge of the
intricacies of XXXXX, you would have all the reasons to be doubtful.

Yet, if it were not 3 of 5, but 7 out of 9 assessors and some of those seven would have web
2.0 profiles (e.g. LinkedIn, Twitter, GitHub, etc) verifiably[^3] attached to
their addresses, it would be a different matter.

Additionally, there is the price that can be adjusted. If an assessee put's up a
lot of money, he can expect the assessors to go through greater lengths for
verifying his skill and maybe ask them to provide some additional material
besides just logging in a good score.

Also, as an outsider, I would be more likely to trust a verdict if the assessors
involved in it were willing to put up large amounts of money, thus signaling
their confidence in coming to the same conclusion as a bunch of other assessors
they didn't know anything about.

#### ...For Scaling

Finally, let's look at what this variability means for the reach of this
project:

As anybody who can define a piece of knowledge solidly enough to allow
individuals to assess it via internet can create a credential and anybody
willing to spent time can participate as assessor, there is no limit as too how
many persons can be assessed and how many subjects can be represented in
fathom-credentials at the same time.

This is a stark contrast to the current certificate landscape, where
certificates have to have a reputation before they can be meaningful. 


## Conclusion: 

In their minimal form, fathom credentials place a strong focus on subjectivity,
so that believing a fathom credential does not mean _"I *trust* that this is the
truth"_, but rather _"I *see* that X people believe this to be the truth and they
were willing to back up that belief with money"_.

Being able to interpret fathom-certificates will not be as straightforward as
conventional certificates, but it will be the same for all kinds of them.
Whether it is a hands-on technological skill or knowledge of a theoretical
concept.

__In the last and final post of that series, I will take a step back and place
fathom-credential and insititutional credentials next to each other to see where
they intersect and where they are unique.__



[^1] For a more elaborate explanation of the redistribution-mechanism see our whitepaper or this explainer post.

[^2] We spent some time thinking about how to facilitate disagreement and allow
newcomers to create competing definitions and put methods in place to encourage better defintions (better here means more helpful to a community)
- 
[^3]: Of course, my certificate becomes more credible if you can learn who my
    assessors were, where they work, etc... The point I am making here is that
    as long as you can be sure that there are different from me, it is not
    strictly necessary condition to being able to trust the certificate.

[^4]: Although we hope that this will only be necessary when initially
    establishing credentials & communities and that it won't be necessary once
    the network has scaled beyond a certain size.
