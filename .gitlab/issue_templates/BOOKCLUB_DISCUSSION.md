This issue-template should serve as a springboard for discussions around
specific parts of ideas from a book or for how they relate to
education/blockchain/fathom/learning/<yourTopic> today.

There is no fixed scope on what an issue-discussion should or must entail.
Depending on whether your thoughts are fully formed, whether you want to
collectively explore a theme or argue against the author's position on
something, you will find different sections of this template differently
helpful.

The headings and comments are there to assist rather than constrain. Use what
you find helpful in making your trains of thought more accessible to others.
Feel free, to reorder, change, drop or adapt the different sections as they make
sense to you and what you want to talk about.

Also, if you haven't already done so, check out our [bookclub-guide](deadlink)
for tips and tools that'll help you to make your efforts available to other
interested readers so that they understand where you're coming from and to
assure that the discussion starts with all being on the same page.

## Booktitle - <topic> 
**<references>**
<!--- Replace <topic> with a concise description of the topic for discussion -->

### Original Sources
<!--- Reference the parts of the text (chapter, section, paragraph, page, line,...) that 
relevant to this discussion. -->
<!--- If you want to use quotes, make sure to mark as such by using qutation marks and/or italics
<!--- If you have personal highlights or notes on the original text, post the links here. -->


### Ideas
<!--- Give a brief summary of the ideas that caught your attention. Check out our
bookclub-guide for some tipps how to make your summary easily digestible and
helpful for others -->

### Response
<!--- Talk about why these parts of the text were important or noteworthy to
you. This can also be the place for personal thoughts and observations, such as
why the text opened up a new perspective to you -->

### Connection
<!--- Make connections to the text and its ideas.--->

<!--- These can be to other texts or books that put this one in perspective but
also to your personal experience, to society at large, education, blockchain,
decentralized systems, or something entirely different. -->

<!--- If you provide links, add a teaser or short reason why and how they
relate, for example: -->

**Example:** The skill-centers he describes remind me of, the [recourse
center](https://www.coursereport.com/schools/recurse-center), a place where
self-directed learners of different backgrounds come together to become better
programmers.

### Questions & Discussion
<!--- This section is for things you wonder about, confuse you, things you
(dis-)agree strongly, things you would like to discuss with others. -->

<!--- Often, the book in question has been written way before the internet, smartphones,
blockchains and truly decentralizing technologies were possible. Therefore, one
interesting question for example is whether these technologies could validate/invalidate the
ideas in the book. -->


## Output...
<!--- Beyond this issue discussion, we want every bookclub-session to result in something (usually we did
blogposts, but that should definetely not be limiting your ideas here). 
Here's the place to explore what this could be. -->


